#[allow(dead_code)]
pub fn problem12() -> u64 {
    let mut curr_tri: u64 = 3;
    let mut num: u64 = 3;
    let mut nr_of_factors: u64;
    loop {
        nr_of_factors = 0;

        for x in 1..(curr_tri as f64).sqrt() as u64 + 1 {
            if curr_tri % x == 0 {

                // All divisors(factors) come in pairs
                nr_of_factors += 2;
            }
        }

        if nr_of_factors >= 500 {
            return curr_tri;
        }

        //println!("Number: {0}, Number of factors: {1}", curr_tri, nr_of_factors);

        curr_tri += num;
        num += 1;
    }
}
use num::bigint::BigUint;

pub fn problem16() -> u64 {
    let mut big_sum: BigUint = BigUint::new(vec![2]);
    let two: BigUint = BigUint::new(vec![2]);
    let nth_power: u64 = 1000;

    for _ in 1..nth_power {
        big_sum = &big_sum * &two;
    }   

    let mut stringified_big_sum: String = big_sum.to_str_radix(10);
    let mut digit_vector: Vec<u32> = Vec::new();

    for _ in 0..stringified_big_sum.len() {
        digit_vector.push(stringified_big_sum.pop().unwrap().to_digit(10).unwrap());
    }

    let mut result: BigUint = BigUint::new(vec![0]);

    for x in digit_vector {
        result = result + x;
    }

    result.to_str_radix(10).parse::<u64>().unwrap()
}
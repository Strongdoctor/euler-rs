use num::bigint::BigUint;
use num::traits::One;

#[allow(dead_code)]
pub fn problem15() -> u64 {

    fn nchoosek(n: u64, k: u64) -> BigUint {
        let mut result: BigUint = BigUint::one();
        for i in 0..k {
            result = (result * (n - i)) / (i + 1);
        }

        result
    }


    struct Square {
        top_left: (u8, u8),
        top_right: (u8, u8),
        bottom_right: (u8, u8),
        bottom_left: (u8, u8),
    }

    let width: u8 = 20;
    let mut squares: Vec<Square> = Vec::new();

    // The grid is built with top left being: 0, y: 0
    for x in 0..width {
        for y in 0..width {
            let square = Square {
                top_left: (x, y),
                top_right: (x + 1, y),
                bottom_right: (x + 1, y + 1),
                bottom_left: (x, y + 1),
            };
            squares.push(square);
        }
    }

    let mut points: Vec<(u8, u8)> = Vec::new();

    // Get all the points from the squares
    for square in squares {
        points.push(square.top_left);
        points.push(square.top_right);
        points.push(square.bottom_right);
        points.push(square.bottom_left);
        //println!("{:?}, {:?}, {:?}, {:?}", square.top_left, square.top_right, square.bottom_right, square.bottom_left);
    }

    // Remove duplicate points
    points.sort();
    points.dedup();

    let paths: u64 = nchoosek(width as u64*2, width as u64).to_str_radix(10).parse::<u64>().unwrap();

    paths
}
extern crate num;
mod problem11;
mod problem12;
mod problem13;
mod problem14;
mod problem15;
mod problem16;

fn main() {
    //println!("The answer to problem 11 is {:?}", problem11::problem11());

    //println!("The answer to problem 12 is {:?}", problem12::problem12());

    //println!("The answer to problem 13 is {:?}", problem13::problem13());

    //println!("The answer to problem 14 is {:?}", problem14::problem14());

    //println!("The answer to problem 15 is {:?}", problem15::problem15());
    
    println!("The answer to problem 16 is {:?}", problem16::problem16());
}

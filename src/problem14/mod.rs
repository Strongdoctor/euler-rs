#[allow(dead_code)]
pub fn problem14() -> u64 {
    let mut n: u64 = 2;
    let mut x: u64;
    let mut topchain: Vec<u64> = Vec::with_capacity(1);
    let mut topnum: u64 = 1;

    while n < 1000000 {
        //println!("n: {}", n);

        // Inner loop, go through x
        x = n;
        let mut chain: Vec<u64> = Vec::new();
        while x > 1 {

            // if x is even.. else if odd
            if x % 2 == 0 {
                x = x / 2;
            } else {
                x = x * 3 + 1;
            }

            //println!("CurrNum: {}", x);
            chain.push(x);
        }

        if chain.len() > topchain.len() {
            topchain = chain;
            topnum = n;
        }

        n += 1;
    }

    topnum
}